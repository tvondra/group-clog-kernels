#!/bin/bash

SAVED_PATH=$PATH
DATADIR=/data/sekondquad/clog-data
RUNS=10
CLIENTS="1 8 16 32 64 128 192"
DURATION=300
SCALE=300

for d in pg-9.6-granular-locking pg-9.6-granular-locking-no-content-lock pg-9.6-group-update pg-9.6-master; do

        echo `date +%s` "running: $d"

        export PATH=/home/sekondquad/clog-builds/$d/bin:$SAVED_PATH

	# make sure there's nothing running
	killall postgres > /dev/null 2>&1

	rm -Rf $DATADIR

	mkdir $d
	cd $d

	which pg_config > which.log 2>&1

	pg_config  > config.log 2>&1

	pg_ctl -D $DATADIR init > init.log 2>&1

	cp ../postgresql.conf $DATADIR

	pg_ctl -D $DATADIR -l pg.log -w start

	psql -c "select * from pg_settings" postgres > settings.log 2>&1

	createdb pgbench

	echo `date +%s` "init"
	pgbench -i -s $SCALE --unlogged-tables pgbench > init-data.log 2>&1

	echo `date +%s` "warmup : start"

	pgbench -c 32 -j 8 -M prepared -T $DURATION pgbench > /dev/null 2>&1

	echo `date +%s` "warmup : done"

	for r in `seq 1 $RUNS`; do

		for c in $CLIENTS; do

			psql -c checkpoint postgres > /dev/null 2>&1

			echo `date +%s` "clients $c run $r : start"

			pgbench -c $c -j 8 -M prepared -T $DURATION pgbench > clients-$c-$r.log 2>&1;

			echo `date +%s` "clients $c run $r : done"

		done;

	done;

	cd ..

	pg_ctl -D $DATADIR stop

done
