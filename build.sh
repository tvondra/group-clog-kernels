echo "kernel patch run clients tps"

runs=10
clients="1 8 16 32 64 128 192"

for k in kernel-3.2.80 kernel-4.5.5; do

	for d in pg-9.6-master pg-9.6-group-update pg-9.6-granular-locking pg-9.6-granular-locking-no-content-lock; do

		for r in `seq 1 $runs`; do

			for c in $clients; do

				tps=`cat $k/pgbench/$d/clients-$c-$r.log | grep excluding | awk '{print $3}'`

				echo $k $d $r $c $tps

			done

		done

	done

done
